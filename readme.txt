Mouse
=====
Left button: Select or use tool
Right button: Move camera
Wheel: Adjust camera zoom

Keyboard shortcuts
==================
F1: Help
F2: Body edit mode
F3: Vertex edit mode
F4: Joint edit mode
F5: Model mode
F12: Preview XML

Ctr-O: Open file
Ctr-S: Save file
Ctr-A: Select all
Ctr-C: Copy
Ctr-X: Cut
Ctr-V: Paste
Ctr-R: Rotate selection (while in "Body edit mode")
Ctr-T: Translate selection (while in "Body edit mode")
Ctr-E: Export to Lua

Del: Delete selection
G: Toggle grid resolution
Q: Insert vertex (while in "Vertex edit mode")
W: New polygon (while in "Vertex edit mode")

Hold Shift: Add to selection
Hold Alt: Remove from selection
Hold Ctrl: Toggle between multiple selected items