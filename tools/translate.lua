--
-- Translates selected bodies
--
TranslateTool = {}
TranslateToolMT = { __index = TranslateTool }

setmetatable(TranslateTool, { __index = Tool })

function TranslateTool:Create(owner)
  local self = Tool:Create(owner, TranslateToolMT)
  return self
end

function TranslateTool:Destroy()
  Tool.Destroy(self)
end

function TranslateTool:Select(selection)
  self.selection = selection
end

function TranslateTool:Deselect()
  self.selection = nil
end

function TranslateTool:MousePress(wx, wy, alt, shift, ctrl)
  local gstate = self.owner.gstate
  local selection = gstate.selection
  -- move clicked body to top of the selection
  local q = gstate:QueryBodies(wx, wy, wx, wy, 4)
  if #q > 0 and alt == false and shift == false then
    local i = table.find(selection, q[1])
    if i then
      gstate:ShiftSelection(i, 1)
    end
  end
  local bpx, bpy = selection[1]:GetPosition()
  self.offsetx = wx - bpx
  self.offsety = wy - bpy
end

function TranslateTool:MouseMove(wx, wy, owx, owy)
  local ox, oy = self.offsetx, self.offsety
  if ox == nil or oy == nil then
    return
  end
  local gstate = self.owner.gstate
  local wpx, wpy = gstate:GetWorldPivot()
  if wpx == nil or wpy == nil then
    gstate:AABBFromSelection()
  end
  -- snap to grid
  local selection = self.selection
  local bpx, bpy = selection[1]:GetPosition()
  local lx, ly = wx - ox, wy - oy
  wx, wy = gstate:ToGrid(lx, ly)
  owx, owy = bpx, bpy
  local wdx, wdy = wx - owx, wy - owy

  -- translate AABB
  gstate:TranslateSelection(wdx, wdy)
end

function TranslateTool:MouseRelease(wx, wy)
  self.offsetx = nil
  self.offsety = nil
  -- compelte translation
  self.owner:set_tool(SelectTool)
end

function TranslateTool:Redraw(dt)
  local f = "Moving selection (%d bodies)"
  local sz = string.format(f, #self.selection)
  local scene = self.owner:get_scene()
  scene:DrawStatus(sz)
end


