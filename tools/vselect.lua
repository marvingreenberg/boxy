--
-- Selects vertices
--
VSelectTool = {}
VSelectToolMT = { __index = VSelectTool }

setmetatable(VSelectTool, { __index = Tool })

function VSelectTool:Create(owner)
  local self = Tool:Create(owner, VSelectToolMT)

  return self
end

function VSelectTool:Destroy()
  Tool.Destroy(self)
end


function VSelectTool:MousePress(wx, wy, alt, shift)
  local owner = self.owner
  local scene = owner:get_scene()
  local gstate = owner.gstate
  local selection = gstate.selection
  local selection2 = gstate.selection2

  local sq = 4*scene:GetZoom()
  local q, s = gstate:QueryVertices(wx, wy, wx, wy, sq)
  if #q > 0 and alt ~= true and shift ~= true then
    local i = table.find(selection, q[1])
    if i then
      gstate:ShiftSelection(i, 1)
    else
      gstate:ClearSelection()
      gstate:AddToSelection({ q[1] }, { s[1] })
    end
    owner:set_tool(VTranslateTool, selection, selection2)
  else
    self.alt, self.shift = alt, shift
    self.sx, self.sy = wx, wy
    
    local gstate = self.owner.gstate
    -- modify or clear selection?
    if alt == true or shift == true then
      self.oldselect = { unpack(gstate.selection) }
      self.oldselect2 = { unpack(gstate.selection2) }
    else
      gstate:ClearSelection()
    end
  end
end

function VSelectTool:MouseMove(wx, wy, owx, owy)
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  local gstate = self.owner.gstate
  local scene = self.owner:get_scene()
  
  local sq = scene:GetZoom()
  local q, s = gstate:QueryVertices(sx, sy, wx, wy, sq)
  gstate:ClearSelection()
  if self.oldselect then
    gstate:AddToSelection(self.oldselect, self.oldselect2)
  end
  if self.alt == true then
    gstate:RemoveFromSelection(q, s)
  else
    gstate:AddToSelection(q, s)
  end
  self.ex, self.ey = wx, wy
end

function VSelectTool:MouseRelease(wx, wy)
  self:MouseMove(wx, wy, wx, wy)
  self.oldselect = nil
  self.oldselect2 = nil
  self.sx, self.sy = nil, nil
  self.ex, self.ey = nil, nil
  self.alt, self.shift = nil, nil
end

function VSelectTool:Redraw(dt)
  local owner = self.owner
  local world = owner:get_world()
  local camera = owner:get_camera()
  local scene = owner:get_scene()
  local gstate = owner.gstate
  if self.ex and self.ey then
    local sx, sy = self.sx, self.sy
    local ex, ey = self.ex, self.ey
    scene:DrawLine(sx, sy, sx, ey, 1, WHITE, 1)
    scene:DrawLine(sx, ey, ex, ey, 1, WHITE, 1)
    scene:DrawLine(ex, ey, ex, sy, 1, WHITE, 1)
    scene:DrawLine(ex, sy, sx, sy, 1, WHITE, 1)
    scene:DrawStatus("Selecting vertices (shift/alt to modify)")
    return
  end
  if #gstate.selection > 0 then
    local f = "%d vertices selected"
    local sz = string.format(f, #gstate.selection)
    scene:DrawStatus(sz)
  else
    scene:DrawStatus("Edit vertices")
    
    -- show tooltip
    local mx, my = mouse.xaxis, mouse.yaxis
    local wx, wy = camera:get_world_point(mx, my)
    local sq = scene:GetZoom()*4
    local v, s = gstate:QueryVertices(wx, wy, wx, wy, sq)
    v = v[1]
    s = s[1]
    if v then
      local t = s:GetType()
      local f = "Vertex \n shape:%s \n lx:%g \n ly:%g"
      local sz = string.format(f, t, v.x, v.y)
      if t == "polygon" or t == "concave" or t == "chain" or t == "loop" then
        local verts = s.def.vertices
        local i = 1
        for i2, v2 in ipairs(verts) do
          if v2 == v then
            i = i2
            break
          end
        end
        local cw = "true"
        if is_clockwise(verts) == false then
          cw = "false"
        end
        local cc = "true"
        if is_convex(verts) == false then
          cc = "false"
        end
        sz = sz .. string.format(" \n index:%d \n clockwise:%s \n convex:%s", i, cw, cc)
      elseif t == "circle" then
        sz = sz .. string.format(" \n radius:%g", s.def.radius)
      end
      scene:DrawTooltip(sz, wx, wy)
    end
  end
end

