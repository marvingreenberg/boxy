gui.OkCancel = function(x, y, title, desc)
  local win = gui.Window(x, y, 300, 85, title)
  win.node.depth = -10
  local label = gui.Label(10, 10, desc, 280)
  label:set_style("align", "left")
  
  win.on_ok = function(win, path)
    
  end
  win.on_cancel = function(win)
    
  end

  local ok = gui.Button(10, 50, "OK", 100)
  local cancel = gui.Button(120, 50, "Cancel", 100)
  ok:set_style("align", "center")
  cancel:set_style("align", "center")
  ok.on_click = function(ok)
    win:on_ok()
  end
  cancel.on_click = function(cancel)
    win:on_cancel()
  end

  win.ok = ok
  win.cancel = cancel

  win:add_child(label)
  win:add_child(ok)
  win:add_child(cancel)
  
  return win
end