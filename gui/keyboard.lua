local lower = 
{
  '', '', '', '', '', '', '', '', '', '',
  '', '', '', '', '', '', '1', '2', '3', '4',
  '5', '6', '7', '8', '9', '0', '-', '=', '', '',
  'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
  '[', ']', '\\', '', 'a', 's', 'd', 'f', 'g', 'h',
  'j', 'k', 'l', ';', '\'', '', '', 'z', 'x', 'c',
  'v', 'b', 'n', 'm', ',', '.', '/', '', '', '',
  '', ' ', '', '', '', '', '', '', '', '',
  '', '', '', '', '', '', '', '', '', '',
  '/', '*', '-', '', '', '', '', '', '', '',
  '', '', '', '', '+', '', '~'
}
local upper =
{
  '', '', '', '', '', '', '', '', '', '',
  '', '', '', '', '', '', '!', '@', '#', '$',
  '%', '^', '&', '*', '(', ')', '_', '+', '', '',
  'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',
  '{', '}', '|', '', 'A', 'S', 'D', 'F', 'G', 'H',
  'J', 'K', 'L', ':', '"', '', '', 'Z', 'X', 'C',
  'V', 'B', 'N', 'M', '<', '>', '?', '', '', '',
  '', ' ', '', '', '', '', '', '', '', '',
  '', '', '', '', '', '', '', '', '', '',
  '/', '*', '-', '1', '2', '3', '4', '5', '6', '7',
  '8', '9', '0', '.', '+', '', '`'
}

-- numpad to non-numpad
local npad =
{
  KEY_NUMDEL = KEY_DEL,
  KEY_NUM_2 = KEY_DOWN,
  KEY_NUM_4 = KEY_LEFT,
  KEY_NUM_6 = KEY_RIGHT,
  KEY_NUM_8 = KEY_UP,
  KEY_NUM_7 = KEY_HOME,
  KEY_NUM_1 = KEY_END,
  KEY_NUM_3 = KEY_PGDOWN,
  KEY_NUM_9 = KEY_PGUP,
  KEY_NUMENTER = KEY_ENTER
} 

local keyboard = keyboard
local base = _G
local string = string
module("gui")

local old_keyboard_on_press = nil
local old_keyboard_on_release = nil
local last_kpress = nil
local time_kpress = nil

-- converts keyboard key code to ansi character
local get_char = function(keyboard, key)
  local nlock = keyboard:is_toggled(base.KEY_NUMLOCK)
  local clock = keyboard:is_toggled(base.KEY_CAPS)
  local shift = keyboard:is_down(base.KEY_LSHIFT) or keyboard:is_down(base.KEY_RSHIFT)

  local up = false
  if key >= base.KEY_NUMLOCK and key <= base.KEY_NUMENTER then
    up = nlock == true
  else
    up = shift == true
  end

  local char = lower[key]
  if up == true then
    char = upper[key]
  end
  -- apply caps lock
  if clock == true then
    if shift == true then
      char = string.lower(char)
    else
      char = string.upper(char)
    end
  end
  return char
end

local keyboard_command = function(keyboard, key)
  last_kpress = key
  time_kpress = 0
  local ctrl = keyboard:is_down(base.KEY_LCTRL) or keyboard:is_down(base.KEY_RCTRL)
  local shift = keyboard:is_down(base.KEY_LSHIFT) or keyboard:is_down(base.KEY_RSHIFT)
  local nlock = keyboard:is_toggled(base.KEY_NUMLOCK)

  local command = false
  local char = get_char(keyboard, key)
  if char == nil or char == '' or ctrl == true then
    -- convert numpad keys to non-numpad keys if numlock is off
    if nlock == false then
      local npkey = npad[key]
      if npkey then
        key = npkey
      end
    end
    command = true
  end
  if command == false then
    input_event('write', char)
  end
  input_event('key_command', key, ctrl, shift)
  
  if old_keyboard_on_press then
    old_keyboard_on_press(keyboard, key)
  end
end

local keyboard_on_press = function(keyboard, key)
  input_event('key_press', key)
  keyboard_command(keyboard, key)
  if old_keyboard_on_release then
    old_keyboard_on_release(keyboard, key)
  end
end

local keyboard_on_release = function(keyboard, key)
  input_event('key_release', key)
  if old_keyboard_on_release then
    old_keyboard_on_release(keyboard, key)
  end
end

-- snatch callbacks from user
local grab_keyboard = function()
  if keyboard == nil then
    return
  end
  if keyboard.on_press ~= keyboard_on_press then
    old_keyboard_on_press = keyboard.on_press
    keyboard.on_press = keyboard_on_press
  end
  if keyboard.on_release ~= keyboard_on_release then
    old_keyboard_on_release = keyboard.on_release
    keyboard.on_release = keyboard_on_release
  end
end
local drop_keyboard = function()
  if keyboard == nil then
    return
  end
  keyboard.on_press = old_keyboard_on_press
  keyboard.on_release = old_keyboard_on_release
  old_keyboard_on_press = nil
  old_keyboard_on_release = nil
end

init_keyboard = function()
  last_kpress = nil
  time_kpress = 0
end
release_keyboard = function()
  last_kpress = nil
  time_kpress = nil
  drop_keyboard()
end

update_keyboard = function(dt)
  grab_keyboard()
  
  -- register repeated keystrokes
  if last_kpress and keyboard:is_down(last_kpress) == true then
    -- key press time
    time_kpress = time_kpress + dt
    local delay = get_style('repeat_delay')
    if time_kpress > delay then
      if time_kpress > delay + get_style('repeat_rate') then
        local ld = last_kpress
        keyboard_command(keyboard, ld)
        time_kpress = delay
        last_kpress = ld
      end
    end
  end
end