local base = _G
local math = math
module("gui")

-- Clipped container object using viewport and layer

VContainerG = {}
VContainerGMT = { __index = VContainerG }

base.setmetatable(VContainerG, { __index = ContainerG })

VContainerG.create = function(x, y, width, height, mt)
  local self = Container(x, y, width, height, mt or VContainerGMT)

  -- center layer at the upper left corner of the viewport
  self.layer:set_position(-width/2, height/2)
  self.viewport = base.Viewport(x, y, width, height)
  self.viewport:add_child(self.layer)
  self.node = self.viewport

  return self
end

VContainerG.destroy = function(self)
  self:destroy_children()

  self.viewport:remove_child(self.layer)
  self.viewport = nil
  ContainerG.destroy(self)
end

-- dimensions accessors
VContainerG.set_position = function(self, x, y)
  self.x = x
  self.y = y
  self.node.x = x
  self.node.y = y
end

VContainerG.set_size = function(self, w, h)
  ContainerG.set_size(self, w, h)
  self.viewport:set_size(w, h)
end

VContainerG.update = function(self, dt)
  -- center layer at the upper left corner of the viewport
  local width, height = self:get_size()
  self.layer:set_position(-width/2, height/2)
  ContainerG.update(self, dt)
end

VContainerG.set_camera = function(self, camera)
  self.viewport.camera = camera
end

VContainerG.get_camera = function(self)
  return self.viewport.camera
end

-- converts local point to camera coords
VContainerG.local_to_camera = function(self, lx, ly)
  local w, h = self:get_size()
  return lx - w/2, h/2 - ly
end

VContainerG.camera_to_local = function(self, wx, wy)
  local w, h = self:get_size()
  return wx + w/2, h/2 - wy
end

VContainer = VContainerG.create