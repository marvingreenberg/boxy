-- plots a rectangle
Canvas.rect = function(self, l, t, r, b)
  -- non-beveled
  local w = math.abs(r - l)
  local h = math.abs(b - t)
  local x = l + w/2
  local y = t + h/2
  self:move_to(x, y)
  self:rectangle(w, h)

  --[[
  -- beveled
  local c = 1
  self:move_to(l, b - c)
  self:line_to(l + c, b)
  self:line_to(r - c, b)
  self:line_to(r, b - c)
  self:line_to(r, t + c)
  self:line_to(r - c, t)
  self:line_to(l + c, t)
  self:line_to(l, t + c)
  self:close_path()
  ]]
end

Canvas.write_line = function(self, font, sz, x, y, width, align)
  local out = font:clamp(sz, width, "...")
  local w = font:get_width(out)
  local ox = 0
  if align == 'center' then
    ox = width/2 - w/2
  elseif align == 'right' then
    ox = width - w
  end
  self:move_to(x + ox, y)
  self:write(out)
end
