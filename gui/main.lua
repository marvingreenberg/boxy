local base = _G
local string = string
local mouse = mouse
local keyboard = keyboard

module("gui")

local dir = "boxy/gui"
local core =
{
  "font", "canvas", "string", "keyboard", "mouse",
  "styles", "object", "container", "vcontainer"
}
for i, v in base.ipairs(core) do
  local filepath = string.format("%s/%s.lua", dir, v)
  base.dofile(filepath)
end

container = nil
timer = nil
clipboard = nil

function create()
  if container then
    return
  end

  init_styles()
  init_colors()
  
  timer = base.Timer()
  timer:start(16, true)
  timer.on_tick = tick

  local v = base.display.viewport
  local w, h = v.width, v.height
  container = VContainer(0, 0, w, h)
  base.display.viewport:add_child(container.node)

  init_mouse()
  init_keyboard()
end

function destroy()
  if container == nil then
    return
  end
  
  base.display.viewport:remove_child(container.node)
  container:destroy()
  container = nil

  timer:stop()
  timer = nil
  
  release_styles()
  release_colors()

  release_mouse()
  release_keyboard()
end

function get_clipboard()
  return clipboard
end
function set_cilpboard(c)
  clipboard = c
end

function input_event(func, ...)
  if container then
    container[func](container, ...)
  end
end

-- update function
function tick(timer)
  if container == nil then
    return
  end

  -- set the root container size to the default viewport
  local v = base.display.viewport
  local w, h = v.width, v.height
  container:set_size(w, h)

  -- delta in seconds
  local dt = timer:get_delta_ms()/1000
  
  -- process repeated keystrokes
  update_keyboard(dt)
  -- process double clicks and such
  update_mouse(dt)
  
  container:update(dt)
end