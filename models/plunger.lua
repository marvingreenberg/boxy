local Plunger = {}
local PlungerMT = { __index = Plunger }

setmetatable(Plunger, { __index = Model })

Plunger.params = { width = 0.25, height = 0.25, speed = 5, force = 10 }

function Plunger:Create(world)
  local self = {}
  setmetatable(self, PlungerMT)
  
  self.world = world

  return self
end

function Plunger:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Plunger:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2

  local w, h = math.abs(dx)/2, math.abs(dy)/2
  local pad = math.min(w/8, h/8)
  local pw, ph = w - pad*2, h - pad

  local world = self.world
  local box = NewBoxPath(w, h)
  local base = world:CreateBodyEx("staticBody", x, y)
  base:CreateShapeEx('polygon', box, 0, 1, 0, true)
  
  local box2 = NewBoxPath(pw, pad*2, 0, -ph + pad)
  local box3 = NewBoxPath(pad, h, pw + pad, 0)
  box3[2].y = box3[2].y + pad*2
  box3[4].y = box3[4].y - pad*2
  local box4 = NewBoxPath(pad, h, -pw - pad, 0)
  box4[1].y = box4[1].y + pad*2
  box4[3].y = box4[3].y - pad*2
  local base2 = world:CreateBodyEx("staticBody", x, y)
  base2:CreateShapeEx('polygon', box2, 0, 1, 0, false)
  base2:CreateShapeEx('polygon', box3, 0, 1, 0, false)
  base2:CreateShapeEx('polygon', box4, 0, 1, 0, false)
  local px, py = x, y + ph + pad
  local platform = world:CreateBodyEx("dynamicBody", px, py, 0, 0, 0, true, true, false, false)
  
  local plw, plh = pw - b2.linearSlop*4, pad*2.5
  local box5 = NewBoxPath(plw, plh)
  platform:CreateShapeEx('polygon', box5, 10, 1, 0, false)

  local s, f = params.speed, params.force*platform:GetMass()

  local p = world:CreateJointEx('prismatic', base, platform, false, px, py + pad*2, 0, 1)

  p:EnableMotor(true)
  p:SetMotorSpeed(-s)
  p:SetMaxMotorForce(f)

  platform:ChangePosition(0, -pad*2.5)

  --world:CreateJointEx('rope', platform, base, false, px, py - pad*2, px, py - ph*2)
  
  local out = { base, base2, platform } 

  local h2 = ph - pad*2
  local n = math.floor(h2/plw) + 1
  
  local retracted, extended = -h*2 + pad*4, 0
  retracted = retracted + (pad*2)*(n + 2)
  p:EnableLimit(true)
  p:SetLimits(retracted, extended)
  
  local dy = h2/n
  local dx = math.sqrt(plw^2 - (dy^2))
  local ang = math.atan2(dy, dx) - math.pi/2

  local bl, br
  local y = py - pad*4
  for i = 0, n do
    local x1, y1 = dx, -dy
    local x2, y2 = -dx, dy
    if i == 0 then
      x2 = 0
      y2 = 0
    elseif i == n then
      x1 = 0
      y1 = 0
    end
    local x3, y3 = -x1, y1
    local x4, y4 = -x2, y2

    local pw,ph, px,py = pad,plw, 0,0
    if i == 0 then
      pw,ph = pw,ph/2
      px,py = 0,-plw/2
    elseif i == n then
      pw,ph = pw,ph/2
      px,py = 0,plw/2
    end
    
    local first = i == 0
    local last = i == n
    local sensor = (first or last)
    
    local b2 = world:CreateBodyEx("dynamicBody", x, y, -ang, 0, 0, false, true)
    b2.def.gravityScale = 0
    b2:CreateShapeEx('polygon', NewBoxPath(pw,ph, px,py), 0.01, 0, 0, true)


    --local b2 = world:CreateBodyEx("dynamicBody", x, y)
    --b2:CreateShapeEx('chain', { {x=x1,y=y1}, {x=x2,y=y2} }, 0.01, 0, 0, true)
    table.insert(out, b2)

    local b3 = world:CreateBodyEx("dynamicBody", x, y, ang, 0, 0, false, true)
    b3.def.gravityScale = 0
    b3:CreateShapeEx('polygon', NewBoxPath(pw,ph, px,py), 0.01, 0, 0, true)

    --local b3 = world:CreateBodyEx("dynamicBody", x, y)
    --b3:CreateShapeEx('chain', { {x=x3,y=y3}, {x=x4,y=y4} }, 0.01, 0, 0, true)
    table.insert(out, b3)

    if not sensor then
      local b2b = world:CreateBodyEx("dynamicBody", x, y, 0, 0, 0, true, true)
      b2b:CreateShapeEx('polygon', NewBoxPath(ph,pad), 0.01, 0, 0, false)
      b2b.def.gravityScale = 0
      world:CreateJointEx('revolute', b2, b2b, false, x, y)
      world:CreateJointEx('revolute', b3, b2b, false, x, y)
      table.insert(out, b2b)
    end
    
    if bl and br then
      local j1 = world:CreateJointEx('revolute', br, b2, false, x+x2,y+y2)
      local j2 = world:CreateJointEx('revolute', bl, b3, false, x+x4,y+y4)
      j1:EnableLimit(true)
      j1:SetLimits(-math.pi - ang*2, 0 - ang*2)
      j2:EnableLimit(true)
      j2:SetLimits(0 + ang*2, math.pi + ang*2)
      --bl:CreateShapeEx('circle', pad, 0.01, 0, 0, false, 0, -plw)
      --br:CreateShapeEx('circle', pad, 0.01, 0, 0, false, 0, -plw)
    end

    if i == 0 then
      local j1 = world:CreateJointEx('revolute', platform, b2, false, x, y)
      local j2 = world:CreateJointEx('revolute', platform, b3, false, x, y)
      --j1:EnableLimit(true)
      --j1:SetLimits(-math.pi/2 + ang, 0 + ang)
      --j2:EnableLimit(true)
      --j2:SetLimits(0 - ang, math.pi/2 - ang)

    elseif i == n then
      local j1 = world:CreateJointEx('revolute', base2, b2, false, x, y)
      local j2 = world:CreateJointEx('revolute', base2, b3, false, x, y)
      j1:EnableLimit(true)
      j1:SetLimits(0 + ang, math.pi/2 + ang)
      j2:EnableLimit(true)
      j2:SetLimits(-math.pi/2 - ang, 0 - ang)
    else
      local j = world:CreateJointEx('revolute', b2, b3, false, x, y)
      --j:EnableLimit(true)
      --j:SetLimits(-math.pi/2 - ang, 0 - ang)
    end
    
    bl, br = b2, b3

    y = y - dy*2
  end

  return out
end

function Plunger:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  if sx == ex and sy == ey then
    local params = self.params
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2

  scene:DrawRectangle(x, y, dx, dy, 1, ORANGE, 1)
end

return Plunger