local Door = {}
local DoorMT = { __index = Door }

setmetatable(Door, { __index = Model })

Door.params = { thickness = 0.05 }

function Door:Create(world)
  local self = {}
  setmetatable(self, DoorMT)
  
  self.world = world

  return self
end

function Door:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Door:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if dx < b2.linearSlop and dy < b2.linearSlop then
  end
  
  local a = math.atan2(dy, dx)
  local len = math.sqrt(dx*dx + dy*dy)
  local params = self.params

  local world = self.world
  local base = world:CreateBodyEx("staticBody", sx, sy, a)

  local t = params.thickness
  --local polygon = { b2Vec2(0, -params.base), b2Vec2(len, -params.tip), b2Vec2(len, params.tip), b2Vec2(0, params.base) }
  local polygon = NewPath(0, -t, len, -t, len - t*2, t, 0, t)
  local platform = world:CreateBodyEx("dynamicBody", sx, sy, a, 0, 0, false, true)
  platform:CreateShapeEx('circle', t, 0, 1, 0, false)
  platform:CreateShapeEx('polygon', polygon, 10, 1, 0)
  --platform:CreateShapeEx('circle', params.tip, 10, 1, 0, false, len, 0)

  local p = world:CreateJointEx('revolute', base, platform, false, sx, sy)
  --[[
  p:EnableLimit(true)
  p:SetLimits(0, math.pi/2)
  p:EnableMotor(true)
  p:SetMotorSpeed(-1)
  p:SetMaxMotorTorque(10)
  ]]

  return { base, platform }
end

function Door:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  local d = math.sqrt(dx*dx + dy*dy)
  local a = math.atan2(dy, dx) + math.pi/2
  local ca = math.cos(a)
  local sa = math.sin(a)

  local params = self.params
  local t = params.thickness

  scene:DrawCircle(sx, sy, t, 1, ORANGE, 1)
  local ax, ay = sx + ca*t, sy + sa*t
  local bx, by = sx + dx/d*(d - t*2) + ca*t, sy + dy/d*(d - t*2) + sa*t
  local cx, cy = sx - ca*t, sy - sa*t
  local dx, dy = ex - ca*t, ey - sa*t
  scene:DrawLine(ax, ay, bx, by, 1, ORANGE, 1)
  scene:DrawLine(cx, cy, dx, dy, 1, ORANGE, 1)
  scene:DrawLine(bx, by, dx, dy, 1, ORANGE, 1)
end

return Door