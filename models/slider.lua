local Slider = {}
local SliderMT = { __index = Slider }

setmetatable(Slider, { __index = Model })

Slider.params = { width = 0.25, height = 0.25, wheels = 2 }

function Slider:Create(world)
  local self = {}
  setmetatable(self, SliderMT)
  
  self.world = world

  return self
end

function Slider:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Slider:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2

  local w, h = math.abs(dx)/2, math.abs(dy)/2
  local a = 0
  if w > h then
    a = math.pi/2
    w, h = h, w
  end
  local world = self.world
  local base = world:CreateBodyEx("staticBody", x, y, a)

  local top = h - w
  local box = NewPath(-w, -top, -w, top, 0, h, w, top, w, -top, 0, -h)
  local platform = world:CreateBodyEx("dynamicBody", x, y, a, 0, 0, true, true, false, false)
  platform:CreateShapeEx('polygon', box, 10, 1, 0, false)

  local ax, ay = math.sin(a), math.cos(a)
  local prism = world:CreateJointEx('prismatic', base, platform, false, x, y, ax, ay)

  prism:EnableLimit(true)
  prism:SetLimits(-h, h)
  
  local objects = { base, platform }
  
  local h2 = h - w*2
  local dx = (h2*2) / (params.wheels - 1)
  local wx, wy = x - h2, y - w
  for i = 1, params.wheels do
    local wheel = world:CreateBodyEx("dynamicBody", wx, wy, a, 0, 0, false, false, false, false)
    wheel:CreateShapeEx('circle', w, 0.05, 0, 0, true)

    local revolute = world:CreateJointEx('revolute', wheel, platform, false, wx, wy)

    local gear = world:CreateJointEx('gear', wheel, platform, false, prism, revolute)
    gear.def.ratio = w
    
    table.insert(objects, wheel)
    
    wx = wx + dx
  end
  
  return objects
end

function Slider:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  if sx == ex and sy == ey then
    local params = self.params
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2

  scene:DrawRectangle(x, y, dx, dy, 1, ORANGE, 1)
end

return Slider