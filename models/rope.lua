Rope = {}
RopeMT = { __index = Rope }

setmetatable(Rope, { __index = Model })

Rope.params = { thickness = 0.1, segment = 0.25 }

function Rope:Create(world)
  local self = {}
  setmetatable(self, RopeMT)

  self.world = world

  return self
end

function Rope:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Rope:Init(sx, sy, ex, ey)
  if sx == ex and sy == ey then
    ex = ex + b2.linearSlop
  end
  
  local params = self.params
  local h = params.thickness
  local w = params.segment
  local world = self.world

  local dx, dy = ex - sx, ey - sy
  local a = math.atan2(dy, dx)
  local d = math.sqrt(dx*dx + dy*dy)
  local nx, ny = dx/d, dy/d
  local segments = math.floor(d/w)
  local bodies = {}
  for i = 0, segments do
    local x = sx + nx*i*w
    local y = sy + ny*i*w
    local b = world:CreateBodyEx("dynamicBody", x, y, a)
    local box = NewBoxPath(w/2, h/2)
    b:CreateShapeEx('polygon', box, 1, 1, 0)
    table.insert(bodies, b)
  end
  local joints = {}
  local ox, oy = nx*w/4, ny*w/4
  for i = 1, #bodies - 1 do
    local b1, b2 = bodies[i], bodies[i + 1]
    local b1px, b1py = b1:GetPosition()
    local b2px, b2py = b2:GetPosition()
    local x1, y1 = b1px + ox, b1py + oy
    local x2, y2 = b2px - ox, b2py - oy
    local j = world:CreateJointEx('distance', b1, b2, false, x1, y1, x2, y2)
    table.insert(joints, j)
  end

  return bodies
end

function Rope:Preview(sx, sy, ex, ey, scale)
  if sx == ex and sy == ey then
    ex = ex + b2.linearSlop
  end

  local params = self.params
  local h = params.thickness
  local w = params.segment
  local dx, dy = ex - sx, ey - sy
  local a = math.atan2(dy, dx)
  local d = math.sqrt(dx*dx + dy*dy)
  local nx, ny = dx/d, dy/d
  local segments = math.floor(d/w)

  for i = 0, segments do
    local x = sx + nx*i*w
    local y = sy + ny*i*w
    scene:DrawRRectangle(x, y, w/2, h/2, a, 1, ORANGE, 1)
  end
end

return Rope