local Sledgehammer = {}
local SledgehammerMT = { __index = Sledgehammer }

setmetatable(Sledgehammer, { __index = Model })

Sledgehammer.params = { handle = 0.05, head = 0.2 }

function Sledgehammer:Create(world)
  local self = {}
  setmetatable(self, SledgehammerMT)

  self.world = world
  self.bodies = {}

  return self
end

function Sledgehammer:Destroy()
  self.world = nil
  self.bodies = nil
end

function Sledgehammer:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if dx < b2.linearSlop and dy < b2.linearSlop then
  end
  
  local a = math.atan2(dy, dx) - math.pi/2
  local len = math.sqrt(dx*dx + dy*dy)
  local params = self.params

  local world = self.world
  local base = world:CreateBodyEx("staticBody", sx, sy, a)

  local box = NewBoxPath(params.handle, len/2, 0, len/2)
  local hammer = world:CreateBodyEx("dynamicBody", sx, sy, a)
  hammer:CreateShapeEx('polygon', box, 0, 1, 0, false)
  --hammer:CreateShape('box', params.head, params.head, 10, 1, 0, false, 0, len)
  local h = params.head
  --local polygon = { b2Vec2(-h, len), b2Vec2(-h, len - h), b2Vec2(h - h/2, len - h), b2Vec2(h, len - h/2), b2Vec2(h - h/2, len) }
  local polygon = NewPath(-h, len, -h, len - h, h - h/2, len - h, h, len - h/2, h - h/2, len)
  hammer:CreateShapeEx('polygon', polygon, 10, 1, 0, false)

  local p = world:CreateJointEx('revolute', base, hammer, false, sx, sy)
  local mass = hammer:GetMass()
  p:EnableLimit(true)
  p:SetLimits(0, math.pi*2)
  p:EnableMotor(true)
  p:SetMotorSpeed(-1)
  p:SetMaxMotorTorque(mass*10)

  return { base, hammer }
end

function Sledgehammer:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  local len = math.sqrt(dx*dx + dy*dy)
  local a = math.atan2(dy, dx) + math.pi/2

  local params = self.params
  local base = params.base
  local tip = params.tip

  scene:DrawRRectangle(sx + dx/2, sy + dy/2, params.handle, len/2, a, 1, ORANGE, 1)
  scene:DrawRRectangle(sx + dx, sy + dy, params.head, params.handle, a, 1, ORANGE, 1)
end

return Sledgehammer