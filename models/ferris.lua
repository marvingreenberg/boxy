local Ferris = {}
local FerrisMT = { __index = Ferris }

setmetatable(Ferris, { __index = Model })

Ferris.params = { radius = 0.25, carts = 10, speed = 1, torque = 10 }

function Ferris:Create(world)
  local self = {}
  setmetatable(self, FerrisMT)
  
  self.world = world

  return self
end

function Ferris:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Ferris:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local r = math.sqrt(dx*dx + dy*dy)

  local world = self.world
  local base = world:CreateBodyEx("staticBody", sx, sy)

  local platform = world:CreateBodyEx("dynamicBody", sx, sy, 0, 0, 0)
  platform:CreateShapeEx('circle', r, 1, 1, 0, true)

  local s, f = params.speed, params.torque*platform:GetMass()
  local p = world:CreateJointEx('revolute', base, platform, false, sx, sy)
  p:EnableMotor(true)
  p:SetMotorSpeed(-s)
  p:SetMaxMotorTorque(f)
  
  --[[
  local w = r*math.pi*2/params.carts
  for i = 1, params.carts do
    local a = math.rad(i/params.carts*360)
    local x = math.cos(a)*r
    local y = math.sin(a)*r
    local tooth = NewBoxPath(w/4, w/2, x, y, a)
    platform:CreateShapeEx('polygon', tooth, 10, 1, 0, false)
  end

  return { base, platform }
  ]]
  local bodies = {}
  local w = r*math.pi*2/params.carts
  for i = 1, params.carts do
    local a = math.rad(i/params.carts*360)
    local x = math.cos(a)*r
    local y = math.sin(a)*r
    local cart = world:CreateBodyEx("dynamicBody", x + sx, y + sy, a - math.pi/2, 0, 0)
    local tooth = NewBoxPath(w/4, w/2 + w/8, 0, w/2)
    cart:CreateShapeEx('polygon', tooth, 1, 1, 0, false)
    local tooth = NewBoxPath(w/2, w/8, w/4, w + w/8)
    cart:CreateShapeEx('polygon', tooth, 1, 1, 0, false)
    table.insert(bodies, cart)
    local j = world:CreateJointEx('revolute', cart, platform, true, x + sx, y + sy)
    j:SetLimits(-math.pi/2, math.pi/2)
    j:EnableLimit(true)
  end
  table.insert(bodies, base)
  table.insert(bodies, platform)

  return bodies
end

function Ferris:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local r = math.sqrt(dx*dx + dy*dy)
  
  scene:DrawCircle(sx, sy, r, 1, ORANGE, 1)
end

return Ferris