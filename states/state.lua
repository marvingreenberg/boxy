GameState = {}
GameStateMT = { __index = GameState }

function GameState:Create(owner, mt)
  local self = {}
  setmetatable(self, mt or GameStateMT)

  self.owner = owner

  return self
end

function GameState:Destroy()
  self.owner = nil
end
--[[
function GameState:MouseMove(wx, wy, owx, owy)
end

function GameState:MousePress(wx, wy)
end

function GameState:MouseRelease(wx, wy)
end

function GameState:KeyPress(key, ctr, shift)
end
]]
function GameState:Redraw()
end

function GameState:Select()
end

function GameState:Deselect()
end


function GameState:CreateProperties(name, properties)
  local owner = self.owner

  local w, pad = 260, 5

  self.props = {}
  for i, v in ipairs(properties) do
    local p = self:AddProperty(v[1], v[2], w, pad)
    p[4].tooltip = v[3]
    table.insert(self.props, p)
  end
  
  local hw = w/2 - pad*2
  local y = pad
  for i, v in ipairs(self.props) do
    y = y + v[4].height + pad
  end
  --local yo = -owner.menu.height - 1

  local ow, oh = owner:get_size()
  local win = gui.Window(ow - w, 0, w, y + pad, name)
  local tw, th = win:get_title_size()
  win:set_position(ow - w, th)
  win.node.depth = -1

  for i, v in pairs(self.props) do
    win:add_child(v[3])
    win:add_child(v[4])
  end

  self.properties = win
end

function GameState:DestroyProperties()
  if self.properties then
    self.properties:destroy()
    self.properties = nil
  end
end

function GameState:ResetProperties()
  local p = self.properties
  if p then
    local tw, th = p:get_title_size()
    local w, h = p:get_size()
    p:set_position(conf.resx - w, th)
  end
end

function GameState:ShowProperties()
  local p = self.properties
  if p then
    local parent = p:get_parent()
    if parent == nil then
      self.owner:add_child(p)
    end
  end
end

function GameState:HideProperties()
  local p = self.properties
  if p then
    local parent = p:get_parent()
    if parent then
      self.owner:remove_child(p)
    end
  end
end

function GameState:AddProperty(label, input, w, pad)
  local hw = w/2 - pad*2
  local x, y = w/2, pad
  for i, v in ipairs(self.props) do
    y = y + v[4].height + pad
  end
  local i
  if input == "number" then
    i = gui.Input(x, y, hw, "")
    i.change_value = function(i, d)
      local n = tonumber(i.text)
      if n then
        i.text = n + d
        i.on_change(i, i.text)
      end
    end
    i.on_change = function(i, v)
      local n = tonumber(v)
      if n == nil then
        return
      end
      self:ChangeProperty(label, input, n) 
    end
    i.key_command = function(i, key, ctrl, shift)
      if key == KEY_UP then
        i:change_value(0.1)
      elseif key == KEY_DOWN then
        i:change_value(-0.1)
      end
      gui.InputG.key_command(i, key, ctrl, shift)
    end
    i.wheel_move = function(i, z)
      if z > 0 then
        i:change_value(0.1)
      elseif z < 0 then
        i:change_value(-0.1)
      end
    end
  elseif input == "boolean" then
    i = gui.Checkbox(x, y, "")
    i.on_change = function(i, v)
      self:ChangeProperty(label, input, v) 
    end
  elseif type(input) == "table" then
    i = gui.List(x, y, hw, #input)
    i.node.depth = -1
    for _, v in ipairs(input) do
      table.insert(i.items, v)
    end
    i.on_change = function(i, v)
      self:ChangeProperty(label, input, i.items[v])
    end
  end
  local l = gui.Label(pad, y, label, hw)
  l:set_style('align', 'right')
  return { label, input, l, i }
end

function GameState:ClearProperties()
  if self.props == nil then
    return
  end
  for i, v in ipairs(self.props) do
    if v[2] == "number" then
      v[4].text = ""
    elseif v[2] == "boolean" then
      v[4].is_checked = false
    elseif type(v[2]) == "table" then
      v[4].index = 0
    end
  end
end
